package com.example.tpun.service;

import com.example.tpun.domain.Product;
import com.example.tpun.domain.Seller;
import com.example.tpun.repository.ProductRepository;
import com.example.tpun.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Service
public class DataLoader {
    SellerRepository sellerRepository;
    ProductRepository productRepository;

    @Autowired
    public DataLoader(SellerRepository sellerRepository, ProductRepository productRepository) {
        this.sellerRepository = sellerRepository;
        this.productRepository = productRepository;
    }

    @PostConstruct
    private void loadData(){
        // create sellers
        Seller seller1 = new Seller("Thibaud","Rue Crémont",31000,"Toulouse",null);
        Seller seller2 = new Seller("Victor","Rue Libun",32000,"Auch",null);
        Seller seller3 = new Seller("Hogo","Rue Mono",15000,"Aurillac",null);
        this.sellerRepository.save(seller1);
        this.sellerRepository.save(seller2);
        this.sellerRepository.save(seller3);

        // create products
        Product product1 = new Product("La C","Rail de 30cm mini","pipo",34.50, LocalDateTime.now(),seller1);
        Product product2 = new Product("La C","Rail de 30cm mini","pipo",34.50, LocalDateTime.now(),seller2);
        Product product3 = new Product("La C","Rail de 30cm mini","pipo",34.50, LocalDateTime.now(),seller3);
        Product product4 = new Product("La dope","Sleepy AF","pipo",14.50, LocalDateTime.now(),seller1);
        Product product5 = new Product("La Thymine","Energetic","pipo",132.00, LocalDateTime.now(),seller2);
        Product product6 = new Product("L'hero","lknzfuzmziz","pipo",1234.00, LocalDateTime.now(),seller3);
        this.productRepository.save(product1);
        this.productRepository.save(product2);
        this.productRepository.save(product3);
        this.productRepository.save(product4);
        this.productRepository.save(product5);
        this.productRepository.save(product6);
    }
}
