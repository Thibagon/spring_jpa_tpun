package com.example.tpun.repository;

import com.example.tpun.domain.Seller;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SellerRepository extends CrudRepository<Seller, Long> {
    @Query(value = "" +
            "SELECT s.name, SUM(p.price) totalPrice " +
            "FROM SELLER s, PRODUCT p " +
            "WHERE p.seller_id=s.id " +
            "GROUP BY s.id " +
            "ORDER BY totalPrice",
            nativeQuery = true)
    List<Seller> findTopSeller();


}
