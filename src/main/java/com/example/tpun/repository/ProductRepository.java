package com.example.tpun.repository;

import com.example.tpun.domain.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findByPriceBetweenOrderByPrice(double price1, double price2);
}
