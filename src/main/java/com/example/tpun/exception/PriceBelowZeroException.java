package com.example.tpun.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class PriceBelowZeroException extends RuntimeException {
    public PriceBelowZeroException(){
        super("Price is below Zero");
    }
}
