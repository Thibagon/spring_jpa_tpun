package com.example.tpun.controller;

import com.example.tpun.domain.Product;
import com.example.tpun.domain.Seller;
import com.example.tpun.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class SellerController {

    @Autowired
    private SellerRepository sellerRepository;

    @GetMapping("/sellers")
    public Iterable<Seller> FindAllSellers(){
        Iterable<Seller> sellers = sellerRepository.findAll();
        return sellers;
    }

    @GetMapping(path = "/bestseller", produces = "application/json")
    public List<Seller> Index(){
        return sellerRepository.findTopSeller();
    }

    @RequestMapping(value = "/addSeller/{name}/{adress}/{zipCode}/{city}", method = GET)
    @ResponseBody
    public String AddSeller(@PathVariable String name, @PathVariable String adress, @PathVariable Integer zipCode,
                            @PathVariable String city) {
        Collection<Product> products = new ArrayList<>();
        Seller seller = new Seller(name,adress,zipCode,city,products);
        seller = sellerRepository.save(seller);
        return "Get some Foos with Header";
    }
}