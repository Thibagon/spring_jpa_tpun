package com.example.tpun.controller;


import com.example.tpun.domain.Product;
import com.example.tpun.exception.PriceBelowZeroException;
import com.example.tpun.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;


    @RequestMapping(value="/products",method = GET, produces = "application/json")
    public Iterable<Product> FindAllProducts(){
        Iterable<Product> products = productRepository.findAll();
        return products;
    }

    @RequestMapping(value="/product/{id}",method = GET,produces = "application/json")
    public Product FindProduct(@PathVariable Long id){
        return productRepository.findById(id).get();
    }

    @GetMapping(path = "/products/{price1}/{price2}", produces = "application/json")
    public List<Product> Index(@PathVariable double price1,@PathVariable double price2) throws PriceBelowZeroException{
        if(price1 < 0 || price2 < 0){
            throw new PriceBelowZeroException();
        }
        return productRepository.findByPriceBetweenOrderByPrice(price1,price2);
    }


}
