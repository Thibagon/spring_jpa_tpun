package com.example.tpun.domain;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.Collection;

@Entity
public class Seller {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;
    @Max(99999)
    private Integer zipCode;
    private String city;
    @OneToMany(mappedBy="seller")
    private Collection<Product> products;

    public Seller(){

    }

    public Seller(String name, String address, @Max(99999) Integer zipCode, String city, Collection<Product> products) {
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Seller{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode=" + zipCode +
                ", city='" + city + '\'' +
                ", products=" + products +
                '}';
    }
}

